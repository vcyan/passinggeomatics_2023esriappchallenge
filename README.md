# PassingGeomatics_2023EsriAppChallenge

# Conservation-Go

By Passing Geomatics

Team Members: Belle Tuen, Deep Singh, Noah Cameron, Victor Yan

University of Waterloo

# Overview

Conservation-Go is game that, much like the phenom of Pokémon-Go back in 2016, Conservation-Go believes in connecting the community. Players can go out into their environment and identify species of animals and trees in their neighbourhood. By doing so they will earn points, and be added to online leaderboards. This encourages players tp explore green-spaces, document wildlife and vegetation, and learn more about the local ecology. Connect with like-minded individuals called 'Green Trainers', and charities that help communities preserve urban ecology. 

# Mission Statement
The Conservation-Go project aims to encourage the exploration of local ecology. An educational tool that helps Green Trainers learn about their neighborhood parks, and the Species Collector App that helps them 'catch' information. There is an abundance of green spaces within the GTA and beyond, much of which may not be known to young individuals. Much like how Pokémon-Go revolutionized interaction, discovery and community, the Conservation-Go journey aims to evoke similar feelings. In which community comes together to learn about their environmental surroundings, species, vegetation and take on initiatives that are relevant to them.


# Conservation-Go Products

Below are all of the products that are apart of Conservation-Go.

## Conservation-Go Online Hub

https://storymaps.arcgis.com/collections/882d871609164bb29b31c43c2c9890dd

The online hub links all of the Conservation-Go hub products together. Every product can be accessed from this page.

## StoryMap Collection

https://storymaps.arcgis.com/collections/882d871609164bb29b31c43c2c9890dd?item=1

The StoryMap Collection is the starting point for all the media associated with Conservation-GO. It provides an overview of our project and goals as well as instructions on how to play the game. It also provides easy to use tabs to access all content (All other products), from the submission forms to the leaderboards.

ArcOnline Site

The Conservation-GO site page details the background of the project. Giving our newest 'Green Trainer'
the rules to play the game. From highlighting the nearest green spaces to the conservation areas, the
Conservation-GO site is the one-stop destination for the newest members.

ArcOnline Experience & Instant Apps

NOAH INSERT STUFF HERE


How to play Conservation-GO


Begin by touring the Conservation-GO ArcOnline Site

Identify your nearest parks/green spaces through the 'Closest Parks Navigator'
	Aim for parks that are nearby to start.

Using the Survey123 Apps collect vegetation and species on your travels.
	Submit photos and point locations to gain points for your team municipality



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.uwaterloo.ca/ntcamero/passinggeomatics_2023esriappchallenge.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://git.uwaterloo.ca/ntcamero/passinggeomatics_2023esriappchallenge/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
